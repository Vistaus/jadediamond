import QtQuick 2.4
import Ubuntu.Components 1.3

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'jadediamond.bhdouglass'
    automaticOrientation: true
    anchorToKeyboard: false // The osk is handled by the KeyboardRectangle since the UI toolkit doesn't respect scaling

    width: units.gu(45)
    height: units.gu(75)

    PageStack {
        id: pageStack
        anchors {
            fill: undefined // unset the default to make the other anchors work
            top: parent.top
            left: parent.left
            right: parent.right
            bottom: keyboardRect.top
        }

        Component.onCompleted: push(Qt.resolvedUrl('BrowserPage.qml'))
    }

    KeyboardRectangle {
        id: keyboardRect
    }
}
